using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PrecipPredictor.Models;
using PrecipPredictor.Contracts;
using PrecipPredictor.Services;

namespace PrecipPredictor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PrecipController : ControllerBase
    {
        private readonly ILogger<PrecipController> _logger;
        private readonly IPredictionSvc _predictionSvc;

        public PrecipController(ILogger<PrecipController> logger, IPredictionSvc predictionSvc)
        {
            _logger = logger;
            _predictionSvc = predictionSvc;
        }


        [HttpGet, Route("/precip/{forecastDate}")]
        public ActionResult<PrecipForecastVM> Get(DateTime forecastDate)
        {
            return _predictionSvc.GetPrediction(forecastDate);
        }


        [HttpGet, Route("/precip")]
        public ActionResult<PrecipForecastVM> Get()
        {
            return Get(DateTime.Now);
        }
    }
}