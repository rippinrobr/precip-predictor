using System;
using System.Collections.Generic;
using PrecipPredictor.Models;

namespace PrecipPredictor.Contracts
{
    /// <summary><c>IReadRepository</c> is used to represent the contract
    /// for all repositories that describe the type of read-only methods on our dataset</summary>
    public interface IReadRepository
    {
        
         IEnumerable<PrecipDTO> Find(DateTime forecastDate);
    }
}