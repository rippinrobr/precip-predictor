using System;
using PrecipPredictor.Models;

namespace PrecipPredictor.Contracts
{
    /// <summary><c>IPredictionSvc</c> is the contract that describes the methods required
    /// by a PredictoinSvc</summary>
    public interface IPredictionSvc
    {
        /// <summary><c>GetPrediction</c> will do the necessary calculations to create a 
        /// precipitation prediction</summary>
        /// <param><c>DateTime forecastDate</c>is the date for which the prediction is needed</param> 
        /// <returns><c>PrecipForecastVM</c> which contains the results of the prediction</returns>
         PrecipForecastVM GetPrediction(DateTime forecastDate);
    }
}