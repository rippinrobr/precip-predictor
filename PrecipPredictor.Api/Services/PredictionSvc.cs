using System.Linq;
using System.ComponentModel;
using System.Net;
using System.Data;

using System;
using PrecipPredictor.Contracts;
using PrecipPredictor.Models;

namespace PrecipPredictor.Services
{

    /// <summary>The <c>PredictionSvc</c> is responsible for generating
    /// precipitation related predictions</summary>
    public class PredictionSvc :IPredictionSvc 
    {
        /// <Summary> This will hold the ReadRepository for the <c>PrecipDataContext</c>.</summary>
        private IReadRepository _repo;
        
        public PredictionSvc(IReadRepository repo) {
            this._repo = repo;
        }

        /// <summary>The <c>GetPrediction</c> method calculates the mean, standard 
        /// deviation, and a prediction for the given date </summary>
        /// <param><c>DateTime forecastDate</c> is the date to create the forecast for</param>
        /// <example> For example:
        /// <code>
        ///     var forecast = svc.GetPrediction(DateTime.Now);
        ///     System.Console.WriteLine("The predicted precip value is " + forecast.Prcp);
        /// </code>
        /// </example>
        /// <returns> a <c>PrecipForecastVM</c> object that contains the results of
        /// calculations.  If no data is found for the given date an empty PrecipForecastVM 
        /// is returned. The caller should be the one to decide what to do with the empty
        /// object.</return>
        public PrecipForecastVM GetPrediction(DateTime forecastDate) {
            var records = _repo.Find(forecastDate);
            // if there are no records I should return right away since there
            // are no values.  I'm not throwing an exception here since there's
            // technically not a a problem, there's just no data. I will leave it 
            // to the caller to decide what the appropriate way to handle this 
            // state. Since the forecastDate has to be a valid date
            // before we get here this shouldn't be true but I don't want to
            // have the potential of a divide by zero error
            if (records.Count() == 0) {
                return new PrecipForecastVM();
            } 
    
            // I know its 'safe' here so I can't start doing m calculations
            var mean = records.Sum(r => r.Prcp) / records.Count();
            var squares = records.Select(r => Math.Pow( r.Prcp - mean, 2));
            var stdDev = squares.Sum(s => s) / squares.Count();
            // I'm going to make a prediction that the precip will be half the
            // standard deviation above the mean but I'm going to provide the caller
            // with the mean, std dev, and my prediction so that they can have the
            // info I used to make my predictions
            var prediction = Convert.ToSingle(mean + (stdDev/2.0));
            
            return new PrecipForecastVM(forecastDate, prediction, mean, stdDev);
        }

        
    }
}