using System.Runtime.Serialization.Formatters;
using System.Runtime.Versioning;
using System.Runtime.Intrinsics.X86;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using PrecipPredictor.DAL;
using PrecipPredictor.Models;
using ExcelDataReader;


namespace PrecipPredictor {
    public class DbLoader {
        public static void Initialize(IServiceCollection services)
        {
            var options =  new DbContextOptionsBuilder<PrecipDataContext>()
                .UseInMemoryDatabase(databaseName: "precip_readings")
                .Options;
            
            // Setting up the database with the data that will be needed to forecast precip values
            using (var db = new PrecipDataContext(options)) {
                using (var stream = new FileStream("27612-precipitation-data.xlsx", FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        var result = reader.AsDataSet();
                        
                        foreach(System.Data.DataRow row in result.Tables[0].Rows.Cast<DataRow>().Skip(1)) {
                            // Getting the Date value from the Excel sheet
                            string date = Convert.ToString(row[5]);
                            // Getting the Prcp values from the Excel Sheet
                            string strPrecip = Convert.ToString(row[6]);   

                            float precip = 0.0f;
                            if (strPrecip != "" && strPrecip != null) {
                                precip = Convert.ToSingle(strPrecip);
                            }    
                            
                            var dto = new PrecipDTO(Convert.ToDateTime(date), precip);
                            db.Add(dto);
                        }                
                    }        
                }

               db.SaveChanges();
            }  

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new PrecipDataContext(options))
            {
                Console.WriteLine("There are " + context.WeatherReadings.Count() + " weather readings.");
            }
        }
    }
}