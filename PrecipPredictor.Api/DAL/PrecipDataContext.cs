using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PrecipPredictor.Models;

namespace PrecipPredictor.DAL {
    /// <summary><c>PrecipDataContext</c> is the layer that provides the data from the database</summary>
    public class PrecipDataContext : DbContext {
        public PrecipDataContext (DbContextOptions<PrecipDataContext> options) : base (options) { }

        internal DbSet<PrecipDTO> WeatherReadings { get; set; }
    }
}