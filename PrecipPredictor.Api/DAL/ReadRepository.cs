using System.Runtime.InteropServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PrecipPredictor.Contracts;
using PrecipPredictor.Models;

namespace PrecipPredictor.DAL
{
    /// <summary> The ReadRepository is responsible for querying the 
    /// the database to retrieve the precip data for the given date</summary> 
    public class ReadRepository : IReadRepository
    {
        private PrecipDataContext _dbCtx;
        
        public ReadRepository(PrecipDataContext ctx) {
            _dbCtx= ctx;
        }

        /// <summary> <c>Find</c> is used to query for all results that occurred 
        /// in the given month on the given day.</summary>
        /// <param><c>DateTime forecastDate</c> is the date we are making a prediction for. The query will
        /// use the month and the day to retreive the data we need.</param>
        /// <returns>IEnumerable<PrecipDTO> that contains the results of the query.async</returns>
        public IEnumerable<PrecipDTO> Find(DateTime forecastDate) {
            return _dbCtx.WeatherReadings
                .Where(r => r.Month == forecastDate.Month && r.Day == forecastDate.Day)
                .OrderByDescending((r1) => r1.Date)
                .ToList();
        }
    }
}