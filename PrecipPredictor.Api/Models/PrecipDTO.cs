using System.Net.Sockets;
using System.Data;
using System;

namespace PrecipPredictor.Models
{
    /// <summary><c>PrecipDTO</c> is used to represent the data 
    /// as it is parsed, saved and read from the database. This class is 
    /// meant for internal use and should not be sent to a client application</summary>
    public class PrecipDTO 
    {
        public long Id {get; set;}

        public uint Month{get; set;}

        public uint Day {get; set;}
        public DateTime Date { get; set; }
        public float Prcp { get; set; }

        public PrecipDTO(DateTime date, float prcp) {
            this.Month = Convert.ToUInt32(date.Month);
            this.Day = Convert.ToUInt32(date.Day);
            this.Date = date;
            this.Prcp = prcp;
        }
    }
}