using System.Security.Principal;
using System;
using System.Linq;
using System.Collections.Generic;

namespace PrecipPredictor.Models
{
    /// <summary><c>PrecipForecastVM</c> models the data that will be sent to the client</summary>
    public class PrecipForecastVM
    {
        public string Date {get; private set;}
        public float Precip {get; private set;}
        public Double Mean  {get; private set;}
        public double StdDev {get; private set;}

        public PrecipForecastVM(DateTime date, float precip, double mean, double stddev) {
            Date = date.ToString("yyyy-MM-dd");
            Precip = precip;
            Mean = mean;
            StdDev = stddev;

        }

        public PrecipForecastVM() {}
    }
}
