using System.Reflection;
using System;
using System.Collections.Generic;
using Xunit;
using Microsoft.EntityFrameworkCore;
using PrecipPredictor.Models;
using PrecipPredictor.DAL;
using PrecipPredictor.Services;
using PrecipPredictor.Contracts;

namespace PrecipPredictor.Test
{
    public class TestEmptyReadRepository : IReadRepository {
        public IEnumerable<PrecipDTO> Find(DateTime forecastDate) {
            return new List<PrecipDTO>();
        }
    }

    public class TestReadRepository : IReadRepository {
        public IEnumerable<PrecipDTO> Find(DateTime forecastDate) {

        PrecipDTO[] data = {new PrecipDTO(DateTime.Now, 0.1f),
                new PrecipDTO(DateTime.Now, 0.3f),
                new PrecipDTO(DateTime.Now, 0.5f),
                };

            return new List<PrecipDTO>(data);
                
        }
    } 

    public class PredictionSvcTest
    {
        [Fact]
        public void TestGetPredictionWithNoRecords_ReturnsEmptyObject()
        {
            var today = DateTime.Now;
            var repo = new TestEmptyReadRepository();
            var service = new PredictionSvc(repo);
            var result = service.GetPrediction(today);

            Assert.Equal(0, result.Precip);
            Assert.Equal(0, result.Mean);
            Assert.Equal(0, result.StdDev);
        }

        [Fact]
        public void TestGetPredictionWithRecords_ReturnsValidObject()
        {
            var today = DateTime.Now;
            var repo = new TestReadRepository();
            var service = new PredictionSvc(repo);
            var result = service.GetPrediction(today);

            Assert.Equal(today, result.Date);
            Assert.Equal(0.31333333253860474, result.Precip);
            Assert.Equal(0.30000001192092896, result.Mean);
            Assert.Equal(0.026666667461395416, result.StdDev);
        }
    }
}
