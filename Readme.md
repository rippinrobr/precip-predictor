## Precip Predictor API
The API provides two HTTP GET routes to give a precipitation prediction for the current day or the date provided by the caller. The response from the API is a JSON object that contains the date for the forecast is for, the amount of precipitation expected, the mean precipitation on that date, and the standard deviation.  
 

## The Routes
### HTTP /precip 
This route will use the current date and return the JSON object with the prediction and statistics.

### HTTP GET /precip/<date>
Similar to the first route, this will return a JSON object with the prediction for the given date.  The date should be in the format YYYY-MM-DD

### JSON Response

```
{
    "date":"2019-09-29",
    "precip":0.49900725,
    "mean":0.42176470160484314,
    "stdDev":0.1544851223141265
}
```

## Running the API & Tests
To start the API run `dotnet run`  in the project's root directory. The server starts up and listens on port 5001 for https and port 5000.  In the same directory running `dotnet test` will execute the project's unit tests.  

## Notes

* I created the API using .Net Core SDK 3.0.100 on Linux
* I decided to load the Excel file's data into an in-memory database so that I can execute queries against it to determine the prediction.  In production, I would not use this method in a production application.
* To make my prediction, I decided to use the precip values for all the stations on the given day of the month.  I averaged the readings and found their standard deviation.  The final prediction was the mean precip for the day, adding half of the standard deviation.  I decided against using other approaches like linear regression.
* I chose to write unit tests for the service layer since that is where the logic lives.  The controller returns what the service returns. In a production application, I would test the controller as well.